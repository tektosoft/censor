# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][],
and this project adheres to [Semantic Versioning][].

## [1.1.2][] - Unreleased

### Added

- Invisible mode.

### Fixed

- Test coverage check.

## [1.1.1][] - 2019-09-23

### Changed

- Moved to Tektosoft repository

## [1.1.0][] - 2019-09-23

### Added

-   `composer_lock` rule.
-   Markdown linting.
-   Spell checking.
-   Message, that everything is cool.

### Changed

-   From now, there are 3 type of builds: `stable` — from master, `latest` — from develop and version tagged like `v1.1.0`.

## [1.0.1][] - 2019-09-22

### Fixed

-   Disabled changelog update check on non–release branches

## [1.0.0][] - 2019-09-22

### Added

-   Working prototype with 7 rules

[keep a changelog]: https://keepachangelog.com/en/1.0.0/

[semantic versioning]: https://semver.org/spec/v2.0.0.html

[1.0.0]: https://gitlab.com/tektosoft/censor/tree/v1.0.0
[1.0.1]: https://gitlab.com/tektosoft/censor/tree/v1.0.1
[1.1.0]: https://gitlab.com/tektosoft/censor/tree/v1.1.0
[1.1.1]: https://gitlab.com/tektosoft/censor/tree/v1.1.1
[1.1.2]: https://gitlab.com/tektosoft/censor/compare/develop...v1.1.1
