import ts from 'rollup-plugin-typescript2';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import builtins from 'rollup-plugin-node-builtins';
import globals from 'rollup-plugin-node-globals';
import typescript from 'typescript';
import pkg from './package.json';

export default [
    {
        input: 'src/index.ts',
        output: {
            banner: '#!/usr/bin/env node',
            file: 'dist/censor.js',
            format: 'cjs',
        },
        external: [
            ...Object.keys(pkg.dependencies),
            'fs',
            'util',
            'path'
        ],
        plugins: [
            globals(),
            builtins(),
            resolve(),
            commonjs(),
            ts({ typescript }),
            terser(),
        ],
    },
]