# Censor

CI tool, that checks typical errors at the GitLab CI stage.

<img src="https://gitlab.com/uploads/tektosoft/censor/7d5830b57f7130a4706affac2a5c9943/image.png" width="906" alt="Merge request commit preview">

## Setup

Simply add the Censor job on merge request to your GitLab CI pipeline.

```yaml
censor:
  image: tektosoft/censor
  stage: review
  only:
    - merge_requests
  script:
    - censor
```

## Configure

By adding a `.censor.json` configuration file to your repository, you can disable or configure the rules.

To disable a rule, give it a false to it's name.

```json
{
  "rules": {
    "jira_task": false
  }
}
```

## Rules

There 7 rules for now:

-   `changelog` — Verifies that the change log is updated during the release;
-   `changes_count` — Checking to make sure the merge request isn't too big. Warns when MR is greater than 400 lines and throws an error when MR is greater than 600;
-   `coverage` — Checking the coverage level with tests. To work with this rule, you should set up the parsing of coverage in GitLab CI;
-   `have_assignee` — Verifies that the MR has an assignee;
-   `jira_task` — Check that the non-release MRs have the jira task number in the title;
-   `labels` — Verifies that labels are assigned to MR;
-   `package_lock` — Check that `package-lock.json` is updated with `package.json`;
-   `composer_lock` — Check that `composer.lock` is updated with `composer.json`;
-   `vendor` — Does not allow to add the `vendor` folder to the repository;
-   `node_modules` — Does not allow to add the `node_modules` folder to the repository.
