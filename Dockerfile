FROM node:lts-alpine
WORKDIR /usr/bin/
RUN npm install gitlab
COPY ./dist/censor.js /usr/bin/censor
