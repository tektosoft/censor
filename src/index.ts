import { validate } from './validator'
import { CensorContext } from './context'
import { GitlabApi } from './gitlab-api'
import { generateMarkdownReport } from './formatters/markdown'
import { printResulets } from './formatters/console'
import { fileExists } from './helpers'
import { join } from 'path'

(async () => {
  const INVISIBLE = process.env.CENSOR_INVISIBLE === 'true'
  if (INVISIBLE) {
    console.log('Censor is running in invisible mode.')
  }

  const api = new GitlabApi(
    process.env.CENSOR_TOKEN,
    process.env.GITLAB_HOST,
    parseInt(process.env.CI_PROJECT_ID),
    parseInt(process.env.CI_MERGE_REQUEST_IID)
  )
  const mergeRequstData = await api.getChangedFiles()
  const context = new CensorContext(
    mergeRequstData
  )
  const configFile = join(process.cwd(), process.argv[2] ? process.argv[2] : '.censor.json')
  const exists = await fileExists(configFile)
  if (exists) {
    const config = require(configFile)
    context.loadConfig(config)
  }

  validate(context)
  const message = generateMarkdownReport(context)
  printResulets(context)
  if (message.trim().length > 0 && !INVISIBLE) {
    await api.addComment(message)
  }
  if (context.errors.length > 0) {
    process.exit(1)
  }
  process.exit(0)
})()
