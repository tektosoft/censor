import { CensorContext } from './../context'
import { Report, ValidationStatus } from 'src/types'

const reset = '\x1b[0m'
const color = {
  red: (string) => `\x1b[31m${string}${reset}`,
  yellow: (string) => `\x1b[33m${string}${reset}`,
  grey: (string) => `\x1b[2m${string}${reset}`,
  green: (string) => `\x1b[32m${string}${reset}`
}

const widthToFixed = (content: string, width: number) => {
  while (content.length < width) {
    content += ' '
  }
  return content
}

const printLine = (report: Report) => {
  let type = ''
  switch (report.type) {
    case ValidationStatus.Error:
      type = 'error'
      break
    case ValidationStatus.Warning:
      type = 'warning'
      break
  }
  console.log(
        `${color.red(widthToFixed(
            type,
            8
        ))} ${report.message} ${color.grey(report.rule)}`
  )
}

export const printResulets = (context: CensorContext) => {
  const errors = context.errors
  const warnings = context.warnings
  for (const error of errors) {
    printLine(error)
  }
  for (const warning of warnings) {
    printLine(warning)
  }

  const count = errors.length + warnings.length
  if (count > 0) {
    const problems = color.red(`${count} problem${count > 1 ? 's' : ''} in total`)
    console.log(`🔥 ${problems}`)
  } else {
    const message = color.green('Everything is cool, keep it up.')
    console.log(`😊 ${message}`)
  }
}
