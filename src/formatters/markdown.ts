import { CensorContext } from './../context'
import { ValidationStatus } from 'src/types'
import { Report } from '../types'

const generateTitle = (status: ValidationStatus, count: number) => {
  let title = ''
  switch (status) {
    case ValidationStatus.Warning:
      title = 'warning'
      break
    case ValidationStatus.Error:
      title = 'error'
      break
  }
  return count > 1 ? `${title}s` : title
}

const generateHeading = (reports: Report[]) => {
  return `| | ${reports.length} ${generateTitle(
        reports[0].type,
        reports.length
    )} |\n`
}

const getSign = (status: ValidationStatus) => {
  switch (status) {
    case ValidationStatus.Warning:
      return '⚠️'
    case ValidationStatus.Error:
      return '❌'
  }
}

const generateTable = (reports: Report[]) => {
  if (reports.length === 0) return ''
  const type = reports[0].type
  let result = generateHeading(reports)
  result += '| ------ | ------ |\n'
  for (const report of reports) {
    result += `|${getSign(type)}| ${report.message} |\n`
  }
  return result
}

export const generateMarkdownReport = (context: CensorContext) => {
  let markdown = ''
  markdown += generateTable(context.errors)
  markdown += '\n\n'
  markdown += generateTable(context.warnings)
  return markdown
}
