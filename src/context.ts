import { Report, ValidationStatus } from './types'
import { mergeDeep } from './helpers'

export class CensorContext {
    private reports: Report[] = []
    private config = {
      release_branches: [
        'master'
      ],
      rules: {}
    }

    public disabledRules = []

    constructor (
        public mergeRequest: any
    ) {
      // console.log(mergeRequest)
    }

    public loadConfig (config: {}) {
      this.config = mergeDeep(
        this.config,
        config
      )
      for (const rule in this.config.rules) {
        if (this.config.rules[rule] === false) {
          this.disabledRules.push(rule)
        }
      }
    }

    public getConfig (ruleName: string): {} {
      return this.config.rules[ruleName] || {}
    }

    public get isRelease (): boolean {
      return this.config.release_branches.includes(this.mergeRequest.target_branch)
    }

    public report (reportContent: Report): void {
      this.reports.push(reportContent)
    }

    public get errors (): Report[] {
      return this.reports
        .filter((report) => report.type === ValidationStatus.Error)
    }

    public get warnings (): Report[] {
      return this.reports
        .filter((report) => report.type === ValidationStatus.Warning)
    }
}
