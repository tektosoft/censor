import * as rules from './rules'
import { CensorContext } from './context'

export async function validate (context: CensorContext) {
  for (const rule in rules) {
    if (context.disabledRules.includes(rule)) continue
    const ruleObject = new rules[rule](rule, context)
    ruleObject.init()
    ruleObject.check()
  }
}
