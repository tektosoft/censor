import { CensorContext } from './context'
import { mergeDeep } from './helpers'
export enum ValidationStatus {
    Error = 0,
    Warning = 1,
}

export interface Report {
    rule: string
    type: ValidationStatus
    message: string
}

export abstract class Rule {
    settings: {}

    constructor (
        protected name: string,
        protected context: CensorContext
    ) {}

    public init () {
      this.settings = mergeDeep(
        this.settings,
        this.context.getConfig(this.name)
      )
    }

    protected warn (message): void {
      this.context.report({
        rule: this.name,
        type: ValidationStatus.Warning,
        message
      })
    }

    protected err (message): void {
      this.context.report({
        rule: this.name,
        type: ValidationStatus.Error,
        message
      })
    }

    abstract check(context) : void;
}
