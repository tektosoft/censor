import { Rule } from './../types'

export default class extends Rule {
  check () {
    if (!this.context.mergeRequest.labels.includes('hotfix')) return
    this.warn('Don\'t forget to back–merge changes to develop branch ☝️.')
  }
}
