import { Rule } from 'src/types'

export default class extends Rule {
    private message = 'The title doesn\'t contain the jira task number 😧. Add it or ask the manager to create the task.'

    private jiraRegexp = /\d+-[A-Z]+(?!-?[a-zA-Z]{1,10})/g

    check () {
      if (this.context.isRelease) return
      const title = this.context.mergeRequest.title
        .split('')
        .reverse()
        .join('')
      const match = title.match(this.jiraRegexp)
      if (match) return
      this.warn(this.message)
    }
}
