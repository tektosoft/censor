import { Rule } from '../types'

export default class extends Rule {
  private message = 'There are vendor folder included in your MR 🤯. Delete it, please.'

  isVendorIncluded () {
    for (const change of this.context.mergeRequest.changes) {
      if (change.new_path.includes('vendor/')) return true
    }
    return false
  }

  check () {
    if (this.isVendorIncluded()) {
      this.err(this.message)
    }
  }
}
