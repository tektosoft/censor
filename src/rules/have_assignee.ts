import { Rule } from 'src/types'

export default class extends Rule {
  check () {
    if (this.context.mergeRequest.assignee) return
    this.err('Merge request haven\'t assignee 😣. ' +
                   'Please assign someone to merge this MR.')
  }
}
