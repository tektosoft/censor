import { Rule } from 'src/types'

export default class extends Rule {
    settings = {
      error: 600,
      warning: 400,
      exclude: [
        'composer.lock',
        'package-lock.json'
      ]
    }

    private isExcluded (path) {
      for (const regexp of this.settings.exclude) {
        const reg = new RegExp(regexp, 'g')
        if (path.match(reg)) return true
      }
      return false
    }

    private get errorMessage () {
      return `Your MR has over ${this.settings.error} lines of code 😱.` + 'Break up into separete MR.'
    }

    private get warningMessage () {
      return `Your MR has over ${this.settings.warning} lines of code 😱.` +
               'Try to break up into separete MR if possible 👍.'
    }

    private get changesCount () {
      const changes = this.context.mergeRequest.changes
      let changesCount = 0
      for (let i = 0; i < changes.length; i++) {
        if (this.isExcluded(changes[i].new_path)) continue
        const diffLines = changes[i].diff.split('\n')
        for (let j = 0; j < diffLines.length; j++) {
          if (diffLines[j][0] === '+') {
            changesCount++
          }
        }
      }
      return changesCount
    }

    check () {
      const changesCount = this.changesCount
      if (changesCount > this.settings.error) {
        this.err(this.errorMessage)
      } else if (changesCount > this.settings.warning) {
        this.warn(this.warningMessage)
      }
    }
}
