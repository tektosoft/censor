import { Rule } from 'src/types'

export default class extends Rule {
    settings = {
      symfony: false
    }

    private lockNotUpdated = 'The composer.json file was updated, but composer.lock wasn\'t 😵. Please execute "rm composer.lock && composer install".'

    private symfonyLockNotUpdated = 'The composer.json file was updated, but symfony.lock wasn\'t ☝️. Check manually if symfony dependencies were updated.'

    private isLockUpdated = false
    private isPackageUpdated = false
    private isSymfonyLockUpdated = false

    private checkFiles () {
      for (const change of this.context.mergeRequest.changes) {
        if (change.new_path.endsWith('composer.lock')) {
          this.isLockUpdated = true
        } else if (change.new_path.endsWith('composer.json')) {
          this.isPackageUpdated = true
        } else if (change.new_path.endsWith('symfony.lock')) {
          this.isSymfonyLockUpdated = true
        }
      }
    }

    check () {
      this.checkFiles()
      if (this.isPackageUpdated && !this.isLockUpdated) {
        this.warn(this.lockNotUpdated)
      } else if (this.isPackageUpdated && !this.isSymfonyLockUpdated) {
        this.warn(this.symfonyLockNotUpdated)
      }
    }
}
