import { Rule } from 'src/types'

export default class extends Rule {
    settings = {
      required: 90
    }

    check () {
      const coverage = parseFloat(this.context.mergeRequest.head_pipeline.coverage)
      if (Number.isNaN(coverage) || coverage > this.settings.required) return
      this.err('Code coverage is too low 😡. Please write more tests.')
    }
}
