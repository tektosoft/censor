import { Rule } from 'src/types'

export default class extends Rule {
    private releaseMessage = 'MR to the master, but no "release" or "hotfix" labels 🤯.'
    private otherMessage = 'MR type is not specified in labels 🤯.'

    settings = {
      release: 'release',
      bugfix: 'bugfix',
      hotfix: 'hotfix',
      feature: 'feature',
      improvement: 'improvement'
    }

    check () {
      const labels = this.context.mergeRequest.labels
      if (this.context.isRelease) {
        if (!labels.includes(this.settings.release) &&
                !labels.includes(this.settings.hotfix)) {
          this.err(this.releaseMessage)
        }
      } else if (!labels.includes(this.settings.bugfix) &&
                   !labels.includes(this.settings.feature) &&
                   !labels.includes(this.settings.improvement)) {
        this.err(this.otherMessage)
      }
    }
}
