import { Rule } from 'src/types'

export default class extends Rule {
    private lockNotUpdated = 'The package.json file was updated, but package-lock.json wasn\'t 😵. Please execute "rm package-lock.json && npm install"'

    private isLockUpdated = false
    private isPackageUpdated = false

    private checkFiles () {
      for (const change of this.context.mergeRequest.changes) {
        if (change.new_path.endsWith('package-lock.json')) {
          this.isLockUpdated = true
        } else if (change.new_path.endsWith('package.json')) {
          this.isPackageUpdated = true
        }
      }
    }

    check () {
      this.checkFiles()
      if (this.isPackageUpdated && !this.isLockUpdated) {
        this.warn(this.lockNotUpdated)
      }
    }
}
