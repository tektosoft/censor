import { Rule } from './../types'

export default class extends Rule {
    settings = {
      file: 'CHANGELOG.md'
    }

    check () {
      if (!this.context.isRelease) return
      let flag = false
      this.context.mergeRequest.changes.forEach(change => {
        if (change.new_path === this.settings.file) flag = true
      })
      if (flag) return
      this.err('Changlog hasn\'t been updated 📄. ' +
                 'Please add a changelog entry for your changes.')
    }
}
