import { Gitlab } from 'gitlab'

export class GitlabApi {
    api: any

    constructor (
      token: string,
      host: string,
        private projectId: number,
        private mergeRequestId: number
    ) {
      this.api = new Gitlab({
        token,
        host
      })
    }

    public getMergeRequestData () {
      return this.api.MergeRequests.show(
        this.projectId,
        this.mergeRequestId
      )
    }

    public getChangedFiles () {
      return this.api.MergeRequests.changes(
        this.projectId,
        this.mergeRequestId
      )
    }

    public addComment (content: string) {
      return this.api.MergeRequestNotes.create(
        this.projectId,
        this.mergeRequestId,
        content
      )
    }
}
