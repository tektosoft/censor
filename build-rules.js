const fs = require('fs')
const { promisify } = require('util')
const { join } = require('path')

const readDir = promisify(fs.readdir)
const writeFile = promisify(fs.writeFile)

readDir(
    join(__dirname, 'src', 'rules')
).then((files) => {
    let result = ''
    for (file of files) {
        const varName = file.split('.')[0]
        result += `export { default as ${varName} } from './rules/${varName}';\n`
    }
    return writeFile(
        join(__dirname, 'src', 'rules.ts'),
        result
    )
})